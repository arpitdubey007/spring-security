package com.ttnd.mvc_mod.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ttnd.mvc_mod.co.UserCO;
import com.ttnd.mvc_mod.services.HomeService;
import com.ttnd.mvc_mod.util.RoleConstant;

@Controller
public class HomeController{
	
	@Autowired
	HomeService homeService;
	
	@RequestMapping(value="/")
	public ModelAndView app() throws IOException{
		homeService.persistsRoles();
		return new ModelAndView("registration","message","Welcome");
	}
	
	@RequestMapping(value="/register")
	public ModelAndView register(@Valid UserCO userCO,BindingResult bindingResult) throws IOException{
		
	String message;
		if(bindingResult.hasErrors()){
			message=bindingResult.getErrorCount()+ ": Error in binding!\n";
			bindingResult.getAllErrors().stream().forEach(error->{
				System.out.println(error+"  :   "+userCO.getEmail());
			});
		}else{
			message="binded successfully!!";
		}
		String role;
		if(userCO.getEmail().contains("rptdby")){
			role=RoleConstant.ADMIN.getRole();
		}else{
			role=RoleConstant.USER.getRole();
		}
		homeService.createUser(userCO,role);
		return new ModelAndView("home","message","Home Page  "+message);
	}
	
	@RequestMapping(value="/login")
	public ModelAndView login(HttpServletResponse response) throws IOException{
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println("Login Page !"+name);
		return new ModelAndView("login","message","Login Page");
	}

	@RequestMapping(value="/user")
	public ModelAndView user(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","Hi User "+homeService.getMessage());
	}

	@RequestMapping(value="/admin")
	public ModelAndView admin(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","Hi Admin "+homeService.getMessage());
	}
	

	@RequestMapping(value="/qa/{msg}")
	public ModelAndView qa(@PathVariable String msg) throws IOException{
		return new ModelAndView("home","message","Hi QA! Message from Dev Team is : "+msg);
	}
	
	@RequestMapping(value="/unauthenticated")
	public ModelAndView unauthenticated(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","Invalid Credentials.");
	}
	
	@RequestMapping(value="/403")
	public ModelAndView accessDeniedPage(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","403 : Unauthorise.");
	}
	
	@RequestMapping(value="/afterlogout")
	public ModelAndView afterlogout(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","Successfully logout.");
	}
	
	@RequestMapping(value="/general")
	//must be authenticated
	public ModelAndView general(HttpServletResponse response) throws IOException{
		return new ModelAndView("home","message","general page");
	}

}
