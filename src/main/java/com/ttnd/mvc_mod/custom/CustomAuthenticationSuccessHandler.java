package com.ttnd.mvc_mod.custom;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	
	public CustomAuthenticationSuccessHandler(){
		super();
		setUseReferer(true);
	}
	
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException {
        System.out.println("Called once per user per login throug out session - CustomAuthenticationSuccessHandler");
        HttpSession session = request.getSession();
        SavedRequest savedReq = (SavedRequest) session.getAttribute(WebAttributes.ACCESS_DENIED_403);
        if (savedReq != null){
        	System.out.println("Seems Unauterise "+savedReq.getRedirectUrl());
        	System.out.println("----------------------------------------");
            response.sendRedirect(savedReq.getRedirectUrl());
        }
        
        String url_prior_login = (String) session.getAttribute("url_prior_login");
        System.out.println("url_prior_login :  "+url_prior_login);
        
        boolean isUser = false;
        boolean isAdmin = false;
        boolean isQa = false;
        for (GrantedAuthority grantedAuthority : auth.getAuthorities()) {
            if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
                isUser = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                isAdmin = true;
                break;
            } else if (grantedAuthority.getAuthority().equals("ROLE_QA")) {
            	isQa = true;
                break;
            }
        }
 
        if (isUser) {
        	response.sendRedirect(request.getContextPath() + "/user");
        } else if (isAdmin) {
        	response.sendRedirect(request.getContextPath() + "/admin");
        } else if (isQa) {
        	System.out.println("Qa person detected.");
        	//response.sendRedirect(request.getContextPath() + "/qa/dump");
        	//this should be requested url.
        }else {
            throw new IllegalStateException();
        }
    }

}