package com.ttnd.mvc_mod.repository;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ttnd.mvc_mod.entity.Role;
import com.ttnd.mvc_mod.entity.User;
import com.ttnd.mvc_mod.util.RoleConstant;

@Repository
public class RoleRepository {
	
	@Autowired
    private SessionFactory sessionFactory;
	
    @Transactional
	public Role findRoleByName(String roleName) {		
		Session session=sessionFactory.openSession();
		Role role = (Role) session.get(Role.class, roleName);
		return role;
	}
    
    @Transactional
	public void persistRoles() {		
		Role role=null;
		for(RoleConstant roleConstant:RoleConstant.values()){
			role=findRoleByName(roleConstant.getRole());
			if(role==null){
				role=new Role();
				role.setRoleName(roleConstant.getRole());
				role.setId(roleConstant.getIndex());
				Session session=sessionFactory.openSession();
				session.saveOrUpdate(role);
				session.flush();
				session.close();
			}
		}
	}

}
