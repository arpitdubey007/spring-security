package com.ttnd.mvc_mod.co;

import java.util.Date;

import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

public class UserCO {
	
	NameCO name;
	int age;
	String email;
	@Size(min=10,max=13,message="phone number must be between {min} and {max} .")
	String phone;
	boolean active;
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	Date dob;
	
	public NameCO getName() {
		return name;
	}
	public UserCO setName(NameCO name) {
		this.name = name;
		return this;
	}
	public int getAge() {
		return age;
	}
	public UserCO setAge(int age) {
		this.age = age;
		return this;
	}
	public String getEmail() {
		return email;
	}
	public UserCO setEmail(String email) {
		this.email = email;
		return this;
	}
	public String getPhone() {
		return phone;
	}
	public UserCO setPhone(String phone) {
		this.phone = phone;
		return this;
	}
	public boolean isActive() {
		return active;
	}
	public UserCO setActive(boolean active) {
		this.active = active;
		return this;
	}
	
	
	

}
