package com.ttnd.mvc_mod.util;

import java.util.Arrays;
import java.util.List;

public enum RoleConstant {

	USER("ROLE_USER", 0), ADMIN("ROLE_ADMIN", 1), SUPER_ADMIN("ROLE_SA", 2), DBA("ROLE_DBA", 3), QA("ROLE_QA", 4), DEV("ROLE_DEV",
			5);

	private String role;
	private int index;

	RoleConstant(String role, int index) {
		this.role = role;
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public String getRole() {
		return this.role;
	}

	public static List<RoleConstant> list() {
		return Arrays.asList(RoleConstant.values());

	}

	public static RoleConstant getByRoleName(String name) {

		switch (name) {
		case "user":
			return RoleConstant.USER;
		case "admin":
			return RoleConstant.ADMIN;
		case "superAdmin":
			return RoleConstant.SUPER_ADMIN;
		case "dba":
			return RoleConstant.DBA;
		case "qa":
			return RoleConstant.QA;
		case "developers":
			return RoleConstant.DEV;
		
		default:
			return null;
		}
	}
}
