package com.ttnd.mvc_mod.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.FilterRegistration.Dynamic;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebXMLReplacer{}



/*extends AbstractAnnotationConfigDispatcherServletInitializer {
 
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(MvcConfig.class);
        ServletRegistration.Dynamic appServlet = servletContext.addServlet("dispatcher", new DispatcherServlet(rootContext));
        servletContext.addListener(new ContextLoaderListener(rootContext));
        appServlet.setLoadOnStartup(1);
        appServlet.addMapping("/");
    }
	
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] { SecurityConfig.class };
	}


	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { MvcConfig.class };
	}
	
	
	@Override
	protected Filter[] getServletFilters() {
		return new Filter[] {
				new CustomFilter()
				, new OpenEntityManagerInViewFilter()
				
				
		};
	}
	
	
	@Override
	public void onStartup(ServletContext container) {
		
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(SecurityConfig.class);

		container.addListener(new ContextLoaderListener(rootContext));

		AnnotationConfigWebApplicationContext displacherContext = new AnnotationConfigWebApplicationContext();
		displacherContext.register(MvcConfig.class);

		ServletRegistration.Dynamic registration = container.addServlet("dispatcher",
				new DispatcherServlet(displacherContext));
		registration.setLoadOnStartup(1);
		registration.addMapping("/");
		
		container.addFilter("springSecurityFilterChain", new DelegatingFilterProxy("springSecurityFilterChain"))
				.addMappingForUrlPatterns(null, false, "/*");

		container.addFilter("customFilter", CustomFilter.class).addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
	}
	
	

	
}*/
