package com.ttnd.mvc_mod.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

@Configuration
public class WebXMLReplacer2 implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) {

		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(SecurityConfig.class);

		container.addListener(new ContextLoaderListener(rootContext));

		AnnotationConfigWebApplicationContext displacherContext = new AnnotationConfigWebApplicationContext();
		displacherContext.register(MvcConfig.class);

		ServletRegistration.Dynamic registration = container.addServlet("dispatcher",
				new DispatcherServlet(displacherContext));
		registration.setLoadOnStartup(1);
		registration.addMapping("/");

		container.addFilter("springSecurityFilterChain", new DelegatingFilterProxy("springSecurityFilterChain"))
				.addMappingForUrlPatterns(null, false, "/*");

		/*container.addFilter("customFilter", CustomFilter.class).addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");*/
	}

}