package com.ttnd.mvc_mod.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;

import java.util.Properties;

import javax.sql.DataSource;


//dependency needed-

//commons-dbcp : BasicDataSource


//spring-orm : LocalSessionFactoryBuilder , HibernateTransactionManager
//hibernate-core : Sessionfactory

//spring-orm's SessionFactoryBuilder returns hibernates Sessionfactory.



@Configuration
public class SpringORMHibernateSupportConfig {
	
	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
	    dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	    dataSource.setUrl("jdbc:mysql://localhost:3306/security_mvc");
	    dataSource.setUsername("root");
	    dataSource.setPassword("root");
	    return dataSource;
	}
	
	//In case of JPA we will configure EntityManagerFactory rather than SessionFactory.
	
	/*@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) {
	 
		//spring-orm This is spring support for hibernate
	    LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
	    //sessionBuilder.addAnnotatedClasses(User.class);
	    sessionBuilder.scanPackages("com.ttnd.mvc_mod.entity");
	    //sessionBuilder.setProperty("hibernate.show_sql", "true");
	    sessionBuilder.addProperties(getHibernateProperties());
	    SessionFactory factory = sessionBuilder.buildSessionFactory();
	    return factory;
	}*/
	
	
	
	
	@Bean
	@Autowired
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setPackagesToScan(new String[] { "com.ttnd.mvc_mod.entity" });
        sessionFactory.setHibernateProperties(getHibernateProperties());
        return sessionFactory;
     }
	
	@Bean
    @Autowired
    public HibernateTemplate getHibernateTemplate(SessionFactory sessionFactory){
        HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);
        return hibernateTemplate;
    }
	
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		//spring-orm This is spring support for hibernate
		return new HibernateTransactionManager(sessionFactory);
	}
	
	private Properties getHibernateProperties() {
	    Properties properties = new Properties();
	    properties.put("hibernate.show_sql", "false");
	    properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	    properties.put("hibernate.hbm2ddl.auto", "update");
	    return properties;
	}

}
