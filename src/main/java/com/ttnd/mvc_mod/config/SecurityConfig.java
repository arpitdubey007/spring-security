package com.ttnd.mvc_mod.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ttnd.mvc_mod.custom.CustomAuthenticationFailureHandler;
import com.ttnd.mvc_mod.custom.CustomAuthenticationSuccessHandler;
import com.ttnd.mvc_mod.services.CustomUserDetailsService;
import com.ttnd.mvc_mod.util.RoleConstant;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@ComponentScan(basePackages = {"com.ttnd.mvc_mod.services","com.ttnd.mvc_mod.repository","com.ttnd.mvc_mod.config","com.ttnd.mvc_mod.custom"})
@Import({SpringORMHibernateSupportConfig.class})
@EnableTransactionManagement
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
    private CustomUserDetailsService userDetailsService;
 
	@Autowired
	CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
	
	@Autowired
	CustomAuthenticationFailureHandler customAuthenticationFailureHandler;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}
	
	@Bean(name="encoder")
	public BCryptPasswordEncoder getPasswordEncoder(){
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		HttpSecurity httpSecurity=http.csrf().disable();
         httpSecurity
        .authorizeRequests()
				.antMatchers("/","/register","/login","/403","/unauthenticated","/afterlogout").permitAll()
				.antMatchers("/user", "/user/**").hasAuthority(RoleConstant.USER.getRole())// hasRole ROLE_ADMIN".
				.antMatchers("/admin", "/admin/**").hasAuthority(RoleConstant.ADMIN.getRole())
				.antMatchers("/qa", "/qa/**").hasAuthority(RoleConstant.QA.getRole())
				.anyRequest().authenticated()
				.and()
			.formLogin()
			     .loginPage("/login")
			     .failureUrl("/unauthenticated")
			     .loginProcessingUrl("/j_spring_security_check")
				 .usernameParameter("j_username")
				 .passwordParameter("j_password")
				 .successHandler(customAuthenticationSuccessHandler)
				 .failureHandler(customAuthenticationFailureHandler)
				.and()
			.logout()
		        .logoutUrl("/j_spring_security_logout")	
			    .logoutSuccessUrl("/afterlogout")
				.and()
			.exceptionHandling().accessDeniedPage("/403");
		       //.formLogin().and().httpBasic();
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
		web.ignoring().antMatchers("/wepapp/resources/**");
	}
	
	
	@Bean @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
	
}
