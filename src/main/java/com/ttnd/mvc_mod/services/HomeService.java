package com.ttnd.mvc_mod.services;

import java.util.Date;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ttnd.mvc_mod.co.UserCO;
import com.ttnd.mvc_mod.entity.Role;
import com.ttnd.mvc_mod.entity.User;
import com.ttnd.mvc_mod.repository.RoleRepository;
import com.ttnd.mvc_mod.repository.UserRepository;

@Service
public class HomeService implements ApplicationContextAware{
	
	ApplicationContext context;
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context=context;
		
	}
	
	@Value("${message}")
	String message;

	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;
	
	
	@Autowired 
	BCryptPasswordEncoder encoder;
	
		
	
	public String getMessage() {
		return "- Welcome!";
	}



	public void createUser(UserCO userCO,String roleString) {
		Role role=roleRepository.findRoleByName(roleString);
		User user= new User();
		user.setUsername(userCO.getEmail().split("@")[0]);
		user.setEmail(userCO.getEmail());
		user.setName(userCO.getName().toString());
		user.setPassword(encoder.encode("123"));
		user.setDob(new Date());
		
		userRepository.createOrUpdateUser(user,role);
	}
	
	public void persistsRoles() {
		roleRepository.persistRoles();
	}

	public String getAuthToken(String uname, String password) {
		User user=userRepository.findByUserName(uname);
		String token="unauthenticated";
		if(BCrypt.checkpw(password,user.getPassword())){
			token=user.getToken().toString();
		}
		return token;
	}
	

}
