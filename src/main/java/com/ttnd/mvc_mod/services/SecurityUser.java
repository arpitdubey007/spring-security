package com.ttnd.mvc_mod.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import com.ttnd.mvc_mod.entity.Role;
import com.ttnd.mvc_mod.entity.User;
import com.ttnd.mvc_mod.repository.UserRepository;

@Component
public class SecurityUser implements UserDetails {
	
	private static final long serialVersionUID = 1784564L;

	@Autowired
	private UserRepository userRepository;
	
	
	Collection<? extends GrantedAuthority> authorities;
	String password;
	String username;
	
	
	public SecurityUser initialiseSecurityUser(String userName){
		User user=userRepository.findByUserName(userName);
		if(user==null){
			return this;
		}
		this.password=user.getPassword();
		this.username=user.getEmail();
		
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		List<Role> userRoles = user.getRoles();
		
		if(userRoles != null){
			for (Role role : userRoles) {
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
				authorities.add(authority);
			}
		}
		this.authorities=authorities;
		
		return this;
		
	}
		
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
